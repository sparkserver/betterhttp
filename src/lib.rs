// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use windows::Win32::Foundation::{HINSTANCE, BOOL};
use windows::Win32::UI::WindowsAndMessaging::{MessageBoxW, MB_ICONERROR};
use windows::core::HSTRING;
use std::ffi::c_void;

mod patches;

#[no_mangle]
extern "system" fn DllMain(
    _dll_module: HINSTANCE,
    call_reason: u32,
    _reserved: *mut c_void
) -> BOOL {
    if call_reason == 1 {
        init();
    }
    true.into()
}

pub fn message_box_error(title: &str, message: &str) {
    let msg_hstring = HSTRING::from(message);
    let title_hstring = HSTRING::from(title);
    unsafe {
        MessageBoxW(None, &msg_hstring, &title_hstring, MB_ICONERROR);
    }
}

fn panic_hook(info: &std::panic::PanicInfo) {
    message_box_error(
        "betterhttp panicked",
        &format!("{}", info),
    );
    std::process::abort();
}

fn init() {
    std::panic::set_hook(Box::new(panic_hook));

    unsafe { patches::apply_patches() }
}
