// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::collections::{HashMap, VecDeque};
use std::ffi::{CStr, CString, c_void};
use async_std::sync::Mutex as AsyncMutex;
use surf::StatusCode;
use surf::http::{mime, Method};
use windows::Win32::System::LibraryLoader::GetModuleHandleA;
use windows::Win32::System::Memory::{VirtualProtect, PAGE_READWRITE, PAGE_PROTECTION_FLAGS};
use lazy_static::lazy_static;
use async_std::task;
use thiserror::Error;

lazy_static! {
    static ref BASE_ADDR: usize = unsafe { GetModuleHandleA(None).unwrap().0 as usize };

    static ref HTTP_CLIENT: surf::Client = {
        surf::Client::new()
    };

    static ref HTTP_STATE: AsyncMutex<HttpState> = {
        let mut headers = HashMap::new();
        headers.insert("User-Agent".to_string(), "betterhttp".to_string());
        AsyncMutex::new(HttpState {
            headers,
            finished_requests: VecDeque::new()
        })
    };

    static ref ORIGINAL_VTABLE: std::sync::Mutex<Option<RestClientVTable>> = std::sync::Mutex::new(None);
}

const ERROR_EMPTY: *const i8 = b"\0".as_ptr() as *const i8;
const ERROR_CONTAINS_NUL: *const i8 = b"(error description contains NUL bytes)\0".as_ptr() as *const i8;

#[derive(Error, Debug, Clone)]
enum RequestError {
    #[error("malformed URL: {0}")]
    MalformedUrl(#[from] url::ParseError),
    #[error("{body}")]
    StatusCode{ code: surf::http::StatusCode, body: String },
    #[error("HTTP request failed: {0}")]
    RequestFailed(String),
    #[error("reading response body failed: {0}")]
    ReadBodyFailed(String),
    #[error("response body contains NUL characters")]
    NulCharInResponse
}

impl RequestError {
    fn error_code(&self) -> i32 {
        match self {
            Self::StatusCode{code, ..} if code == &StatusCode::Unauthorized => 10,
            Self::StatusCode{code, ..} => *code as i32,
            _ => 5,
        }
    }
}

struct FinishedRequest {
    token: i32,
    result: Result<Option<CString>, RequestError>
}

struct HttpState {
    pub headers: HashMap<String, String>,
    pub finished_requests: VecDeque<FinishedRequest>
}

type CallbackType = unsafe extern "fastcall" fn(this: *mut c_void, _: usize, token: i32, error_code: i32, error_string: *const i8, response: *const i8);

#[allow(non_snake_case)]
extern "fastcall" fn RestClient__Poll(this: *mut c_void) -> i8 {
    let callback = unsafe {
        *(this.offset(432) as *mut *mut *const CallbackType)
    };

    let maybe_req: Option<FinishedRequest> = task::block_on(async {
        HTTP_STATE.lock().await.finished_requests.pop_front()
    });

    if let Some(req) = maybe_req {
        if !callback.is_null() {
            match req.result {
                Ok(body) => {
                    let body_ptr = body.as_ref().map_or(std::ptr::null(), |s| s.as_ptr());
                    unsafe {
                        (**callback)(callback as *mut c_void, 0, req.token, 0, ERROR_EMPTY, body_ptr);
                    }
                },
                Err(error) => {
                    let maybe_error_cstring = CString::new(error.to_string()).ok();
                    let error_ptr = maybe_error_cstring.as_ref()
                        .map_or(ERROR_CONTAINS_NUL, |s| s.as_ptr());
                    unsafe {
                        (**callback)(callback as *mut c_void, 0, req.token, error.error_code(), error_ptr, b"\0".as_ptr() as *const i8);
                    }
                }
            }
        }

        1
    } else {
        /*if let Some(vtable) = ORIGINAL_VTABLE.lock().unwrap().as_ref() {
            (vtable.poll)(this)
        } else {
            0
        }*/
        0
    }
}

#[repr(C)]
struct GString {
    pub start: *const u8,
    pub end: *const u8
}

unsafe fn gstring_to_string(str: *const GString) -> String {
    let sl = std::slice::from_raw_parts((*str).start, (*str).end.offset_from((*str).start) as usize);
    String::from_utf8_lossy(sl).to_string()
}

#[allow(non_snake_case)]
unsafe extern "fastcall" fn RestClient__SetHeader(this: *mut c_void, _: usize, key: *const GString, value: *const GString) {
    let (key_str, value_str) = unsafe {
        (gstring_to_string(key), gstring_to_string(value))
    };

    task::block_on(async {
        HTTP_STATE.lock().await.headers.insert(key_str, value_str);
    });

    if let Some(vtable) = ORIGINAL_VTABLE.lock().unwrap().as_ref() {
        (vtable.set_header)(this, 0, key, value);
    }
}

fn check_request_status(status: surf::http::StatusCode, body: String) -> Result<String, RequestError> {
    if status.is_success() {
        Ok(body)
    } else {
        Err(RequestError::StatusCode{ code: status, body })
    }
}

async fn do_request(method: surf::http::Method, uri: &str, body: Option<String>) -> Result<Option<CString>, RequestError> {
    let url = surf::Url::parse(uri)?;
    let mut req = surf::RequestBuilder::new(method, url);

    for (key, value) in HTTP_STATE.lock().await.headers.iter() {
        req = req.header(key.as_str(), value);
    }

    if let Some(body) = body {
        req = req.body_string(body).content_type(mime::XML);
    }

    let mut response = HTTP_CLIENT.send(req).await
        .map_err(|e| RequestError::RequestFailed(e.to_string()))?;
    
    let response_body = response.body_string().await
        .map_err(|e| RequestError::ReadBodyFailed(e.to_string()))?;
    
    let response_body = check_request_status(response.status(), response_body)?;
    
    if response_body.len() > 0 {
        let cstr = CString::new(response_body)
            .map_err(|_| RequestError::NulCharInResponse)?;

        Ok(Some(cstr))
    } else {
        Ok(None)
    }
}

unsafe fn request_impl(this: *mut c_void, uri: *const i8, method: surf::http::Method, body: Option<*const GString>) -> i32 {
    let uri_string = unsafe {
        let str = CStr::from_ptr(uri);
        String::from_utf8_lossy(str.to_bytes()).to_string()
    };

    let body_string = body.map(|body| unsafe {
        gstring_to_string(body)
    });

    let token = unsafe {
        let token_ptr = this.offset(428) as *mut i32;
        let current_token = *token_ptr;
        (*token_ptr) += 1;
        current_token
    };

    task::spawn(async move {
        let result = do_request(method, &uri_string, body_string).await;

        HTTP_STATE.lock().await.finished_requests.push_back(FinishedRequest {
            token,
            result,
        });
    });

    token
}

#[allow(non_snake_case)]
unsafe extern "fastcall" fn RestClient__GetRequest(this: *mut c_void, _: usize, uri: *const i8) -> i32 {
    request_impl(this, uri, Method::Get, None)
}

#[allow(non_snake_case)]
unsafe extern "fastcall" fn RestClient__DeleteRequest(this: *mut c_void, _: usize, uri: *const i8) -> i32 {
    request_impl(this, uri, Method::Delete, None)
}

#[allow(non_snake_case)]
unsafe extern "fastcall" fn RestClient__PostRequest(this: *mut c_void, _: usize, uri: *const i8, body: *const GString) -> i32 {
    request_impl(this, uri, Method::Post, Some(body))
}

#[allow(non_snake_case)]
unsafe extern "fastcall" fn RestClient__PutRequest(this: *mut c_void, _: usize, uri: *const i8, body: *const GString) -> i32 {
    request_impl(this, uri, Method::Put, Some(body))
}

#[repr(C)]
#[derive(Clone)]
struct RestClientVTable {
    pub set_response_callback: *mut c_void,
    pub fn_0058c0b0: *mut c_void,
    pub do_get_request: unsafe extern "fastcall" fn (this: *mut c_void, _: usize, uri: *const i8) -> i32,
    pub do_delete_request: unsafe extern "fastcall" fn (this: *mut c_void, _: usize, uri: *const i8) -> i32,
    pub do_post_request: unsafe extern "fastcall" fn (this: *mut c_void, _: usize, uri: *const i8, body: *const GString) -> i32,
    pub do_put_request: unsafe extern "fastcall" fn (this: *mut c_void, _: usize, uri: *const i8, body: *const GString) -> i32,
    pub set_header: unsafe extern "fastcall" fn (this: *mut c_void, _: usize, key: *const GString, value: *const GString),
    pub fn_0058d5b0: *mut c_void,
    pub fn_0058c250: *mut c_void, 
    pub poll: unsafe extern "fastcall" fn (this: *mut c_void) -> i8,
}

unsafe impl Send for RestClientVTable {}

pub unsafe fn apply_patches() {
    let vtable_ptr = (*BASE_ADDR + 0x7873b8) as *mut RestClientVTable;

    let mut old_protect = PAGE_PROTECTION_FLAGS(0);
    VirtualProtect(vtable_ptr as *const c_void, std::mem::size_of::<RestClientVTable>(), PAGE_READWRITE, &mut old_protect);

    let mut vtable: RestClientVTable = std::ptr::read_volatile(vtable_ptr);

    *ORIGINAL_VTABLE.lock().unwrap() = Some(vtable.clone());
    
    vtable.do_get_request = RestClient__GetRequest;
    vtable.do_delete_request = RestClient__DeleteRequest;
    vtable.do_post_request = RestClient__PostRequest;
    vtable.do_put_request = RestClient__PutRequest;
    vtable.set_header = RestClient__SetHeader;
    vtable.poll = RestClient__Poll;

    std::ptr::write_volatile(vtable_ptr, vtable);

    VirtualProtect(vtable_ptr as *const c_void, std::mem::size_of::<RestClientVTable>(), old_protect, &mut old_protect);
}
